package com.gymnotes2.gymnotes2.Exercise;

import com.gymnotes2.gymnotes2.Training.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepo  extends JpaRepository<Exercise, Long> {
}
