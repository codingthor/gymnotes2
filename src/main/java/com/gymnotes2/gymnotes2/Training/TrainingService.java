package com.gymnotes2.gymnotes2.Training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class TrainingService {


    @Autowired
    private TrainingRepo trainingRepository;

    public List<Training> getAllTrainings() {
        return trainingRepository.findAll();
    }

    public Training getTrainingById(Long id) {
        Optional<Training> trainingOptional = trainingRepository.findById(id);
        return trainingOptional.orElse(null);
    }

    public ResponseEntity<Object> createTraining(Training training) {
        try {
            Training savedTraining = trainingRepository.save(training);
            Long trainingID = savedTraining.getId();

            // Erstelle ein Objekt, das die ID und die Nachricht enthält
            Map<String, Object> response = new HashMap<>();
            response.put("id", trainingID);
            response.put("message", "Training erfolgreich hinzugefügt");


            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            Map<String, Object> response = new HashMap<>();
            response.put("id", null);
            response.put("message", "Fehler beim Hinzufügen des Trainings");

            System.err.println("Fehler beim Hinzufügen des Trainings: " + e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    public Training updateTraining(Long id, Training updatedTraining) {

        Optional<Training> trainingOptional = trainingRepository.findById(id);
        if (trainingOptional.isPresent()) {
            updatedTraining.setId(id);
            return trainingRepository.save(updatedTraining);
        }
        return null;
    }

    public void deleteTraining(Long id) {
        trainingRepository.deleteById(id);
    }
}
