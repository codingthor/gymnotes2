package com.gymnotes2.gymnotes2.Training;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "training")
@Entity
public class Training {

    @Id
    @SequenceGenerator(
            name = "training_sequence",
            sequenceName = "training_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "training_sequence"
    )
    private Long id;
    private String bodyparts;
    private LocalDate date;
    private LocalTime start_time;
    private Integer exercises;

    public Training(){

    }

    public Training(String bodyparts, LocalDate date, LocalTime start_time, Integer exercises) {
        this.bodyparts = bodyparts;
        this.date = date;
        this.start_time = start_time;
        this.exercises = exercises;
    }



    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", bodyparts='" + bodyparts + '\'' +
                ", date=" + date +
                ", duration=" + start_time +
                ", exercises=" + exercises +
                '}';
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyparts() {
        return bodyparts;
    }

    public void setBodyparts(String bodyparts) {
        this.bodyparts = bodyparts;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalTime start_time) {
        this.start_time = start_time;
    }

    public Integer getExercises() {
        return exercises;
    }

    public void setExercises(Integer exercises) {
        this.exercises = exercises;
    }



}
