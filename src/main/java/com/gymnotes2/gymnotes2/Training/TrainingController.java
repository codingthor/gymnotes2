package com.gymnotes2.gymnotes2.Training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trainings")
public class TrainingController {

    @Autowired
    private TrainingService trainingService;

    // Endpunkt für das Abrufen aller Trainings
    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<Training>> getAllTrainings() {
        List<Training> trainings = trainingService.getAllTrainings();
        return new ResponseEntity<>(trainings, HttpStatus.OK);
    }

    // Endpunkt für das Abrufen eines bestimmten Trainings anhand der ID
    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<Training> getTrainingById(@PathVariable("id") Long id) {
        Training training = trainingService.getTrainingById(id);
        return new ResponseEntity<>(training, HttpStatus.OK);
    }

    // Endpunkt für das Erstellen eines neuen Trainings
    @CrossOrigin
    @PostMapping
    public ResponseEntity<Object> createTraining(@RequestBody Training training) {

       return trainingService.createTraining(training);
    }

    // Endpunkt für das Aktualisieren eines vorhandenen Trainings
    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<Training> updateTraining(@PathVariable("id") Long id, @RequestBody Training training) {
        Training updatedTraining = trainingService.updateTraining(id, training);
        return new ResponseEntity<>(updatedTraining, HttpStatus.OK);
    }

    // Endpunkt für das Löschen eines Trainings anhand der ID
    @CrossOrigin
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTraining(@PathVariable("id") Long id) {
        trainingService.deleteTraining(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
