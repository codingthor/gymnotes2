Mit diesem Projekt habe ich das Erlernte aus meinem Master vertieft und zusätzlich mich in die React Frontend
Entwicklung eingearbeitet. Die gesamte Anwendung ist containerisiert und wird durch ein Docker-compose file orchestriert.
--> Frontend: React
--> Backend: SpringBoot
--> DB: MySQL

Anwendungsfall soll eine App sein, um die absolvierten Workouts zu notieren. 
Gedacht war, dass man Workouts erstellen kann, welche wiederum beliebig viele Übungen enthalten. Allerdings ist mir, als 
React-Rookie, die Frontend-Entwicklung schnell auf die Füße gefallen. Deshalb habe ich mich in meiner API zuerst nur auf
die Workouts konzentriert. Ich denke um die 2. Klasse (Übungen) auch zu berücksichtigen, würde ich mit dem Frontend (erneut) 
komplett neu anfangen. So oder so habe ich zumindest interaktive Dropdowns :D 

Nicht alle Aspekte des Frontends sind perfekt aber dennoch genügt es um eine kleine ReST-API (Level 3) anzusprechen. 

Hinweise:
1. Den Wald vor lauter Bäume... Warum auch immer, habe ich zu Beginn den BEgriff "Trainings" statt Workouts genutzt. 
   Als mir dieser Hick-up aufgefallen ist, war das Kind schon in Brunnen gefallen. Daher Konsistenz --> Alles im BAckend heißt
   "Trainings". Im frontend zumindest dann workouts. 
2. Der obere Blaue Button "Update Training" ist nicht zu Ende entwickelt, und generell auch nicht gut überlegt. Wurde dann zum neagtiv Testen der PUT-Method genutzt.
    Updaten erfoglt über die hellgrünen runden Buttons im "Show Workout" Tab
3. Delete_Emthod über die roten Buttons im "Show Workout" Tab
4. Die Übungsinputs im "Add Workout" Tab sind ohne Funktion.


